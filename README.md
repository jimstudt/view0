# View0 – A Simple Template System for Lua

---
**Note:** Consider this to be alpha software. I am still making incompatible changes with no consideration of anyone but myself.

---

View0 is a simple template system which acts as the **view** in an MVC system. You provide View0 with a model and template and it returns to you formatted text.

By "simple" I mean:

* Ships as a single source file with about 12 pages of code.
* Minimal dependencies. (Needs LPEG, but that saves a lot of parsing code. Sorry.)
* API has a single call with one string argument.
* Only six directives to memorize.
* MIT licensed to match Lua itself.
* One of these statements is not quite true.

## The Manual

Go read the manual in the source, `view0.md`, or peruse this small example.

## An HTML Example
Let's say you have a system with users and each user has some details. Maybe you want to have a user detail page. You will need a model. Models are nothing more than Lua tables mapping names to values.

Pretend we pulled this from our database.

```
model = { username='Jim',
          lastseen='Dec 6, 2010',
          privileges={superuser=true,upload=true}
        }
```

Now we are going to need a template. Pretend we keep this in a file named `userdetail.html`

```
<html>
  <header>
    <title>User Detail – «username»</title>
  <header>  
  <body>
    <h3>User «username»</h3>
    <div>Last seen on «last seen».</div>
	«if privileges.superuser»
    <div>Has superuser privileges.</div>
    «end»
  </body>
</html>
```
If we read our template into `userDetailTemplate` then we can…

```
require(View0)

local myViewer = View0.expander(userDetailTemplate)
print( myViewer(model))
``` 
…to get this…

```
<html>
  <header>
    <title>User Detail – Jim</title>
  <header>  
  <body>
    <h3>User Jim</h3>
    <div>Last seen on Dec 6, 2010.</div>
    <div>Has superuser privileges.</div>
  </body>
</html>
```

