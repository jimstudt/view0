#!/usr/bin/env lua
--
-- This is an example for the View0 template expander.
--
-- This contains the model, the controller, and the template 
-- all in one file for your viewing pleasure.
--

require('view0')

local template = [===[
      This is our generated text.
      Our model was named ‹‹‹name››› and had the following inventory:
      ‹‹‹for i in inventory›››
          ‹‹‹i.quantity›››, ‹‹‹i.description›››
      ‹‹‹end›››          
]===]

local model = { name='MyStore',
		inventory={ {quantity=1, description='Unique Item'},
			    {quantity=2, description='Matched set of foo'},
			    {quantity=10, description='Apple'}},
		yes=true,
		no=false }

local view,defects = view0.expander(template)

if defects then
   print('Template was defective:\n'..table.concat(defects,'\n'))
else
   print( view(model))
end