#!/usr/bin/env lua
--
-- This is is intended to fully exercise the View0 template engine
--
-- This contains the model, the controller, and the template 
-- all in one file for your viewing pleasure.
--

view0 = require('view0')

local template = [===[
  Delimiters
      Default UTF *-POINTING DOUBLE ANGLE QUOTATION MARK "«vfoo»"
      Three ASCII angle brackets "<<<vfoo>>>"

  Comments
      «¶  This is a comment»

  Values
      String literal insertion "«'literal'»"
      Simple string insertion "«vfoo»"
      Simple number insertion "«v42»"
      true insertion "«vtrue»"
      false insertion "«vfalse»"
      func insertion "«vfunc»"
      nested func insertion "«vfunc2»"
      insane func insertion "«vfuncn»"
      array insertion "«varray»"
      array insertion with separators "«varray;','»"
      table insertion "«vtable»"
      table value "«vtable.a»"
      complicated insertion "«vcomplicated»"
      recursive insertion "«vlooped»"
      literal OR "«'a' or 'b'»"
      literal OR "«vfoo or 'b'»"
      literal OR "«vfalse or 'b'»"
      literal OR "«vfalsefunc or 'b'»"

  Conditionals
    «if vtrue»
      'if true' was responsible for this line
    «end»
    «if vfalse»
      'if false' was responsible for this line
    «end»
      true is «?vtrue:'istrue'»
      false is «?vfalse:'isfalse'»
      true is «?vtrue:'true':'false'»
      false is «?vfalse:'true':'false2'»
      true is «?vtrue{»«}»
      true is «?vtrue{»a true value«}»
      false is «?vfalse{»a false value«}»
      true again «?vtrue{»
	true is still true
     «}»
    «if v42 = '42'»
      v42 is equal to '42'
    «end»
    «if v42 = '43'»
      v42 is equal to '43'
    «end»
    «if vtrue and true»
      vtrue and true is true
    «end»
    «if vtrue and false»
      vtrue and false is true
    «end»

  Selection
    «select»
    «end»
    «select»
      «otherwise»
         Select with just an otherwise.
         Select with just an otherwise.
    «end»
    «select»
    «when vfalse»
      Selection one chose vfalse path
    «when vtrue»
      Selection one chose vtrue path
    «when true»
      Selection one chose true path
    «end»
    «select»
    «when vfalse»
      Selection two chose vfalse path
    «otherwise»
      Selection two chose otherwise path
    «end»

  Iteration
    «for e in varray»
        just verbatim
    «end»
    «for e in varray»
    «end»
    «for a in varray»
      varray contains «a»«,»
    «end»
      varray contains «*a:varray{»«a»«,»«}»

   Defines and Calls
   «define decl(t,n,v)»
      «t» *«n» = «v»
   «end»
   «=first(a,b){»«a»«}»
   «=paren(x){»(«x»)«}»
   «=dparen(x){»(«paren(x)»)«}»
   «=dota(t){»«t.a»«}»
      «include decl(vfoo,'myvar',v42)»
      «include decl(vfoo,'myvar2','6')»
      Some inline calls, two sad blank lines because I can not decide how to handle that.
      «=quote2(inner,quote){»«quote»«inner»«quote»«}»
      «=quote(inner){»"«inner»"«}»
      «quote2(vfoo,'"')»
      «quote(vfoo)»
      Some calls to external functions
      «allcap('thisWasCamelCase')»
      «mungeStrings(MODEL,'-')»
      «paren('single paren foo')»
      «dparen('double paren foo')»
      «first('first','second')»
      «paren(first('first','second'))»
      «for q in varrayoftable»
      Iteratred table selection gives «dota(q)»
      «end»
      End of calls

   Dump
      «include dump(vcomplicated)»
      «include dump(vlooped)»
]===]


local looped = { a='a',
		 b={'b'},
		 d='d'}
looped.c = looped

local model = { vfoo='foo',
		vthis='this',
		v42=42,
		vtrue=true,
		vfalse=false,
		vfalsefunc=function() return false end,
		vfunc=function() return "a func made this" end,
		vfunc2=function() return function() return 'inner' end end,
		vfuncn=function() 
			  local foo
			  function foo() return foo end 
			  return foo
		       end,  -- infinite recursion here
		varray={'a','b','c'},
		vtable={ a=1, b=2, c=3 },
		varrayoftable= { {a=1,b=2,c=3}, {a=4,b=5,c=6} },
		vcomplicated={ {'a','b','c'},
			       'd',
			       function() return {'e','f'} end,
			       { letter='g'},
			       { letter=function() return 'h' end},
			    },
		vlooped=looped,
		allcap=function(txt) return string.upper(txt) end,
		mungeStrings=function(model,sep)
				local all = {}
				for k,v in pairs(model) do 
				   if type(v)=='string' then table.insert(all,v) end
				end
				return '['..table.concat(all,sep)..']'
			     end,
		dump=view0.dump
	     }

local view,defects = view0.expander(template, { ['«'] = {'»'},	['<<<'] = {'>>>'} } )

if defects then
   print('Template was defective:\n'..table.concat(defects,'\n'))
else
   local r,e = view(model)
   if e then 
      print('Template failed to expand:\n'..table.concat(e,'\n'))
   else
      print(r)
   end
end

