--
-- TL;DR -> MIT licensed, go wild.
--
--      Copyright (c) 2013 Jim Studt
--
--      Permission is hereby granted, free of charge, to any person obtaining
--      a copy of this software and associated documentation files (the
--      "Software"), to deal in the Software without restriction, including
--      without limitation the rights to use, copy, modify, merge, publish,
--      distribute, sublicense, and/or sell copies of the Software, and to
--      permit persons to whom the Software is furnished to do so, subject to
--      the following conditions:
--
--      The above copyright notice and this permission notice shall be
--      included in all copies or substantial portions of the Software.
--
--      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
--      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
--      MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
--      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
--      LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
--      OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
--      WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- A template engine patterned after
--   "Enforcing Strict Model-View Separation in Template Engines" -- Terence Parr
--
-- You should find a view0.md file with the documentation in it.
--
-- Provides:
--    view0.expander( model, template)
--
--

local view0 = {}

local lpeg = require "lpeg"

--
-- A handy utility for the templates to use when they are confused about
-- the structure of their parameters. It recursively dumps the values so
-- you can try to sort it out.
--
local function dump(x,tag)
   local r = {}
   local tableStack = {}

   local function tableid(t)
      local r = '  ['..tostring(t)..']'
      if tag then
	 tt = t[tag]
	 if tt then r = r..' ('..tt..')' end
      end
      return r
   end

   local function dumpitable(x,prefix)
      tableStack[x] = true
      if x[0] then
	 for k,v in ipairs(x) do 
	    if type(v) == 'table' then
	       if tableStack[v] then
		  table.insert(r,prefix..tostring(k)..'= recursive reference to '..' ['..tostring(v)..']')
	       elseif next(v)==nil then
		  table.insert(r,prefix..tostring(k)..'= {}'..tableid(v))
	       else
		  table.insert(r,prefix..tostring(k)..'= {'..tableid(v))
		  dumpitable(v,prefix..'  ')
		  table.insert(r,prefix..'}')
	       end
	    else
	       table.insert(r,prefix..tostring(k)..'= '..tostring(v)) 
	    end
	 end
      else
	 for k,v in pairs(x) do 
	    if type(v) == 'table' then
	       if tableStack[v] then
		  table.insert(r,prefix..tostring(k)..': recursive reference to '..' ['..tostring(v)..']')
	       elseif next(v)==nil then
		  table.insert(r,prefix..tostring(k)..': {}'..tableid(v))
	       else
		  table.insert(r,prefix..tostring(k)..': {'..tableid(v))
		  dumpitable(v,prefix..'  ')
		  table.insert(r,prefix..'}')
	       end
	    else
	       table.insert(r,prefix..tostring(k)..': '..tostring(v)) 
	    end
	 end
      end
      tableStack[x] = false
   end
   
   if type(x)=='table' then
      table.insert(r,'{'..' ['..tostring(x)..']')
      dumpitable(x,'  ')
      table.insert(r,'}')
   else
      table.insert(r,tostring(x))
   end
   table.insert(r,'')
   return table.concat(r,'\n')
end

local function parse( text, delimiters)
   local function evalToString( t, sep, diag)
      local tableStack={}

      local function textrep(x,level)
	 -- easy ones first
	 if type(x) == 'string' then return x end
	 if type(x) == 'number' then return tostring(x) end
	 if not x then return '' end
	 if x == true then return '' end   -- not really a good idea for a default

	 -- we may recurse below here, have a way to fail
	 if not level then level = 1 end
	 if level > 100 then 
	    return '!!!Encountered insane recursion evaluating: "'..diag()..'"!!!'
	 end

	 -- if it is a function, call it and deal with the result
	 if type(x) == 'function' then return textrep( x(), level+1 ) end

	 -- if it is a table, we look for a __tostring method, failing that a __call,
	 -- if none of those then we do a concatenation
	 if type(x) == 'table' then
	    if tableStack[x] then
	       return '!!! Attempt to expand recursive structure: "'..diag()..'"!!!'
	    end
	    tableStack[x] = true

	    local mt = getmetatable(x)
	    
	    if mt and mt.__tostring then return tostring(x) end
	    if mt and mt.__call then return textrep( x(), level+1 ) end

	    -- concatenate an array, with a possible separator
	    local result = ''
	    local sep = sep or ''
	    local first = true
	    local iter
	    if x[1] then 
	       for k,v in ipairs(x) do
		  if first then first = false else result = result..sep end
		  result = result..textrep(v)
	       end
	    else
	       for k,v in pairs(x) do
		  if first then first = false else result = result..sep end
		  result = result..textrep(v)
	       end
	    end

	    tableStack[x] = false;
	    return result
	 end

	 -- what? a thread? userdata? I give up.
	 return ''
      end

      return textrep(t)
   end

   local function evalToBool( t, diag)
      local tableStack={}

      local function boolrep(x,level)
	 -- easy ones first
	 if x==nil then return false
	 elseif type(x) == 'string' then return true
	 elseif type(x) == 'number' then return true 
	 elseif type(x) == 'boolean' then return x 
	 end

	 -- we may recurse below here, have a way to fail
	 if not level then level = 1 end
	 if level > 100 then 
	    return '!!!Encountered insane recursion evaluating: "'..diag()..'"!!!'
	 end

	 -- if it is a function, call it and deal with the result
	 if type(x) == 'function' then return boolrep( x(), level+1 ) end

	 -- if it is a table, we look for a __call and use that, if not we are true
	 if type(x) == 'table' then
	    if tableStack[x] then
	       return '!!! Attempt to expand recursive structure: "'..diag()..'"!!!'
	    end
	    tableStack[x] = true

	    local mt = getmetatable(x)
	    
	    if mt and mt.__call then return boolrep( x(), level+1 ) 
	    else return true
	    end
	 end

	 -- what? a thread? userdata? I give up.
	 return true
      end

      return boolrep(t)
   end

   local function expand( env, func, diag)
      if type(func) ~= 'function' then
	 print('Bad evaluator in expand(): '..diag(), dump(v))
	 return nil,{'Got a non-function in expand: '..diag()}
      end
      local r,e = func(env)
      if e then return nil,e end
      local s = evalToString(r,nil, function() return 'expanding' end)
      return s,nil
   end

   local function getContext( pos)
	 local prefix = string.sub(text,1,pos-1)
	 local _,lineNumber = string.gsub( string.sub(prefix,1,pos), "\n","\n")
	 local ctx = string.match( text, '[^\n]*', pos ) or ""
	 local pctx = string.reverse( string.match( string.reverse(prefix), '[^\n]*' ) or "")

	 return lineNumber,pctx..'^'..ctx
   end

   local function basicMessage(pos,kind)
      local ln,ctx = getContext(pos)
      return (kind or 'Error')..' at line '..ln..': '..ctx 
   end
   
   local function genOp( pos, kind)
      local op = { isOp=true,
		   position=pos,
		   kind=kind,
		}
      op.diagnostic = function() return basicMessage(pos,kind) end
      op.validate=function(env) assert( type(env)=='table', op.diagnostic()) end
      return op
   end

   local function newScope(env) 
      local newscope = {}
      setmetatable( newscope, { __index=env } )
      return newscope
   end

   local function genNewScopeOp( innerOp)
      local op = genOp( innerOp.position, 'New Scope')
      op.evaluator = function(env)
			op.validate(env)

			return innerOp.evaluator(newScope(env))
		     end
      return op
   end

   local function genNewModelOp( innerOp)
      local op = genOp( innerOp.position, 'New Model')
      op.evaluator = function(env)
			op.validate(env)
			local ns = newScope(env)
			ns['MODEL'] = env
			ns['dump'] = dump
			return innerOp.evaluator(ns)
		     end
      return op
   end


   -- make an error result at position with diagnostic string 'msg'.
   -- has a format(self) method that returns a diagnostic string
   local function genError( pos, msg)
      local function format(e)
	 local lineNumber,ctx = getContext(e.position)
	 return tostring(e.text) .. ' line:'..lineNumber..' text:'..ctx
      end
      return { error=true, position=pos, text=msg, format=format}   -- not callable to prevent accidents
   end

   local function unimplemented( name, pos)
      local op = genOp(pos, 'Unimplemented')
      op.evaluator = function(env)
			op.validate(env)
			return nil,{'Unimplemented: '..name}
		     end
      return op
   end

   local function genLookup( pos, id)
      local op = genOp( pos, 'Reference')
      op.evaluator = function(env)
			op.validate(env)
			if op.scope and op.scope[id] then return op.scope[id],nil end
			if env and env[id] then return env[id],nil end
			return nil,nil
		     end
      return op
   end

   local function genVerbatim( pos, txt)
      local op = genOp( pos, 'Verbatim')
      op.evaluator = function(env) 
			op.validate(env)
			return txt,nil 
		     end
      return op
   end

   local function genNL( pred, pos)
      local op = genOp( pos, 'Newline')
      op.evaluator = function(env) 
			op.validate(env)
			local t,e = pred.evaluator(env)
			if e then return nil,e end
			return t..'\n',nil
		     end
      return op
   end

   local function genLiteral( pos, lit)
      local op = genOp( pos, 'Literal')
      op.evaluator = function(env) 
			op.validate(env)
			return lit,nil 
		     end
      return op
   end

   local function genReferenceOp( lhs, pos, field)
      local op = genOp(pos, 'Reference operation')
      op.evaluator = function(env)
			op.validate(env)
			local left,lerrors = lhs.evaluator(env)
			if lerrors then return nil,lerrors end
			if type(left) ~= 'table' then return nil,{'Tried to .'..field..' into a non table: \n'..op.diagnostic()..'\n'..dump(left) } end
			return left[field]
		     end
      return op
   end

   local function genTrue( pos) return genLiteral(pos,true) end
   local function genFalse( pos) return genLiteral(pos,false) end

   local function genBinOp( lhs, pos, operator, rhs)
      local op = genOp(pos, 'Binary operation')
      if operator == 'or' then
	 op.evaluator = function(env)
			   op.validate(env)
			   local left,lerrors = lhs.evaluator(env)
			   if lerrors then return nil,lerrors end
			   if left then return left,nil end
			   local right,rerrors = rhs.evaluator(env)
			   if rerrors then return nil,rerrors end
			   if right then return right,nil end
			   return false,nil
			end
      elseif operator == 'and' then
	 op.evaluator = function(env)
			   op.validate(env)
			   local left,lerrors = lhs.evaluator(env)
			   if lerrors then return nil,lerrors end
			   if not left then return false,nil end
			   local right,rerrors = rhs.evaluator(env)
			   if rerrors then return nil,rerrors end
			   if right then return true,nil end
			   return false,nil
			end
      elseif operator == '=' then
	 op.evaluator = function(env)
			   op.validate(env)
			   local left,lerrors = lhs.evaluator(env)
			   if lerrors then return nil,lerrors end
			   local right,rerrors = rhs.evaluator(env)
			   if rerrors then return nil,rerrors end

			   if type(left) ~= type(right) then
			      if tostring(left) == tostring(right) then return true,nil end
			   else
			      if left == right then return true,nil end
			   end
			   return false,nil
			end
      elseif operator == ';' then
	 op.evaluator = function(env)
			   op.validate(env)
			   local left,lerrors = lhs.evaluator(env)
			   if lerrors then return nil,lerrors end
			   local right,rerrors = rhs.evaluator(env)
			   if rerrors then return nil,rerrors end

			   local sep = evalToString( right,nil,function() return 'some binary semicolon' end)

			   return evalToString( left, sep, function() return 'some binary semicolon' end), nil
			end
      else
	 op.evaluator = function(env)
			   op.validate(env)
			   return nil,{'Unimplemented binary operation: "'..operator..'"' }
			end
      end
      return op
   end

   local function genMulti(pos, ops)
      local op = genOp(pos, 'Statements')
      op.evaluator = function(env)
			op.validate(env)
			if not ops then return '',nil end

			local result = {}
			for _,v in ipairs(ops) do
			   if not v.evaluator then
			      print('Bad evaluator in genMulti', dump(v))
			      return nil,{'Got a non-op in genMulti'}
			   end
			   local r,e = v.evaluator(env)
			   if e then return nil,e end
			   local s = evalToString(r,nil, function() return 'somewhere' end)
			   --print('did '..tostring(r)..'->'..s)
			   table.insert( result, s)
			end
			return table.concat( result), nil
		     end
      return op
   end

   local function genCall(pos,name,vals)
      local op = genOp(pos, 'Call')
      op.evaluator = function(env)
			op.validate(env)
			local func = env[name]
			if not func then return nil, {'"'..name..'" is undefined:'..op.diagnostic()} end 
			if type(func) ~= 'function' then return nil, {'"'..name..'" is not callable:'..op.diagnostic()} end

			local expanded = {}
			for _,v in ipairs(vals) do
			   local exp,err = v.evaluator(env)
			   if err then return nil,err end
			   table.insert(expanded,exp)
			end

			return func( unpack(expanded))
		     end
      return op
   end

   local function genInclude(pos,name,vals)
      return genCall(pos,name,vals)
   end


   local function genCond(pos,cond,iftrue,iffalse)
      local op = genOp(pos, 'Conditional')
      op.evaluator = function(env)
			op.validate(env)
			local cc,e = cond.evaluator(env)
			if e then return nil,e end
			if evalToBool( cc, function() return 'Evaluating condition' end) then
			   return expand(env,iftrue.evaluator, op.diagnostic)
			else
			   if iffalse then return expand(env,iffalse.evaluator, op.diagnostic)
			   else return '',nil
			   end
			end
		     end
      return op
   end

   local function genIf(pos,cond,body)
      return genCond(pos, cond, body, nil)
   end

   local function genSelect( pos, whens, otherwise, crap)
      local op = genOp(pos, 'Select')
      local cases = {}
      for _,v in ipairs(whens) do
	 local cond,body = unpack(v)
	 table.insert( cases, { cond.evaluator, body} )
      end
      if otherwise then         -- add a when that is always true for the otherwise
	 table.insert( cases, { function(env)return true end, otherwise })
      end

      op.evaluator = function(env)
			op.validate(env)
			for _,v in ipairs(cases) do
			   local cond = v[1]
			   local body = v[2]
			   local cc,e = cond(env)
			   if e then return nil,e end
			   if evalToBool( cc, op.diagnostic) then
			      return expand(env, body.evaluator, op.diagnostic)
			   end
			end
			return '',nil
		     end
      return op
   end

   local function genIterator( pos, varname, expr, body)
      local op = genOp(pos, 'Iterator')
      op.evaluator = function(env)
			op.validate(env)
			local domain,err = expr.evaluator(env)
			if err then return nil,err end
			if not domain then return '',nil end
			if type(domain) ~= 'table' then 
			   return nil,'Iteration on non-array: '..op.diagnostic()
			end

			local results = {}
			local errors = nil

			local function accumulate( val, delimit)
			   env[varname] = val
			   env['DELIMIT'] = delimit
			   local r,e = body.evaluator(env)
			   if e and not errors then 
			      errors = e 
			      return
			   end
			   table.insert( results, r)
			end

			-- a little messy to support the delimiters
			local prev
			for _,v in ipairs(domain) do
			   if prev then accumulate( prev,true) end
			   prev = v
			end
			if prev then accumulate( prev, false) end

			if errors then return nil,errors end

			return table.concat(results),nil
		     end
      return genNewScopeOp(op)        -- we work in a new scope so our variable doesn't destroy
   end

   local function genFor( forPart, body)
      return genIterator( forPart[1], forPart[2], forPart[3], body)
   end

   local function genDefine( pos, name, parameters, body)
      local op = genOp(pos, 'Definition')
      op.evaluator = function(env)
			op.validate(env)
			env[name] = function(...)
				       local scope = newScope(env)   -- Important! This is the block scope instead of dynamic scope
				       local arguments = {...}
				       for k,v in ipairs(parameters) do   -- do better, check for length mismatch
					  scope[v] = arguments[k]
				       end
				       return body.evaluator(scope)
				    end
			return '',nil
		     end
      return op
   end

   local function genComma(pos)
      local op = genOp(pos,'Comma')
      op.evaluator = function(env)
			op.validate(env)
			if env['DELIMIT'] then return ',',nil
			else return '',nil
			end
		     end
      return op
   end

   local function genComment(pos)
      local op = genOp(pos,'Comment')
      op.evaluator = function(env)
			op.validate(env)
			return '',nil
		     end
      return op
   end

   local function DefineGrammar( delimiters)
      local g = { 'Everything' }
      local P = lpeg.P
      local C = lpeg.C
      local Cb = lpeg.Cb
      local Cc = lpeg.Cc
      local Cf = lpeg.Cf
      local Cg = lpeg.Cg
      local Cp = lpeg.Cp
      local Ct = lpeg.Ct
      local V = lpeg.V
      local R = lpeg.R
      local S = lpeg.S

      local function forget(...)
	 return
      end
      
      local function Conjoin( t)
	 local r = nil
	 for _,v in ipairs(t) do
	    if r then r = r + P(v) else r = P(v) end
	 end
	 return r
      end
      
      local function keys(t)
	 local r = {}
	 for k,_ in pairs(t) do table.insert(r,k) end
	 return r
      end
      
      g.SDel = Conjoin( keys(delimiters) )

      local allEnds = ( function()
			   local accum = {}
			   for k,v in pairs(delimiters) do
			      for _,e in ipairs(v) do accum[e]=true end
			   end
			   return keys(accum)
			end) ()
      g.EDel = Conjoin( allEnds)

      local function DEL(p)
	 local r = nil
	 for k,v in pairs(delimiters) do
	    local g = P(k) * p * Conjoin(v)
	    if r then r = r + g else r = g end
	 end
	 return r
      end
      
      local function LEM(p)
	 return (V('Verbatim')/forget)^-1 * DEL(p) * (V('Verbatim')/forget)^-1 * V('NL')
      end

      local function K(p)
	 return P(p) * -(V('Alphanum') + P('_'))
      end

      -- a verbatim section with an optional trailing newline, or just a newline by itself.
      -- a verbatim section, stopped by newlines
      g.Verbatim = Cp() * C( (1 - (V('SDel')+V('NL')))^1 )/genVerbatim

      g.NL = P('\n')
      g.Space = S(' \t') ^0
      local SP = V('Space')

      -- used to exclude them from identifiers, Don't miss one!
      g.Keywords = K('if') + K('end') + K('and') + K('or') + K('select') + K('when') + K('otherwise') + K('for') + K('in') + K('define') + K('true') + K('false')

      g.Alphanum = R('az') + R('AZ') + R('09')

      g.Id = C( V('Alphanum') ^ 1 - V('Keywords')) * SP
      
      g.Literal = ("'" * C( (1-P("'"))^0) * "'" +
	        '"' * C( (1-P('"'))^0) * '"') * SP

      g.Atom = V('Call') 
               + ( Cp() * V('Id'))/genLookup 
               + ( Cp() * V('Literal'))/genLiteral
	       + ( Cp() * K('true'))/genTrue
	       + ( Cp() * K('false'))/genFalse

      g.AtomOp = P('.')/'.' * SP
      g.Factor = Cf( V('Atom') * Cg( Cp() * P('.') * V('Id'))^0, genReferenceOp)

      g.FactorOp = P(';')/';' * SP

      g.Term = Cf( V('Factor') * Cg( Cp() * V('FactorOp') * V('Factor'))^0, genBinOp)

      g.Value = V('Term')
      
      g.RelationOp = ( P('=')/'=' + P('!=')/'noequalop' )  * SP
      g.Relation = Cf( V('Value') * Cg( Cp() * V('RelationOp') * V('Value'))^0, genBinOp)
      
      g.ConjunctionOp = K('and')/'and' * SP
      g.Conjunction = Cf( V('Relation') * Cg( Cp() * V('ConjunctionOp') * V('Relation'))^0, genBinOp)
      
      g.DisjunctionOp = K('or')/'or' * SP
      g.Disjunction = Cf( V('Conjunction') * Cg( Cp() * V('DisjunctionOp') * V('Conjunction'))^0, genBinOp)
      
      g.Expr = V('Disjunction') 
      
      g.AtomicConditional = DEL( Cp() * P('?') * V('Expr') * P(':') * V('Expr') * ( P(':') * V('Expr') )^-1 ) / genCond
      g.SpanningConditional = ( DEL( Cp() * P('?') * V('Expr') * P('{')) * V('InlineContent') * V('ClosingBrace') )/genIf
      
      g.Iterator = ( DEL( Cp() * P('*')*V('Id')*SP*P(':')*SP*V('Expr')*P('{')) *V('InlineContent')*V('ClosingBrace'))/genIterator
      
      g.InlineDefine = (DEL( Cp() * P('=')*V('Id')*V('VarList')*P('{'))*V('InlineContent')*V('ClosingBrace'))/genDefine
      
      g.InlineDirective = V('SpanningConditional') 
                        + V('AtomicConditional') 
                        + V('Iterator')
                        + V('InlineDefine')
                        + DEL(V('Expr')) 
                        + DEL(Cp()*P(','))/genComma

      g.Include = LEM( (Cp() * K('include') *SP * V('Id') * SP * V('ExprList') * SP )/genInclude )
      g.End = LEM( K('end') )/forget
      g.ClosingBrace = DEL( P('}'))

      g.ExprList = Ct( P('(') * SP * ( V('Expr') * ( SP * P(',') * SP * V('Expr') )^0 * SP )^-1 * P(')') )
      g.Call = ( Cp() * V('Id') * SP * V('ExprList') * SP )/genCall

      g.VarList = Ct( P('(') * SP * ( V('Id') * ( SP * P(',') * SP * V('Id') )^0 * SP )^-1 * P(')') )

      g.Define = ( LEM( Cp() * K('define')*SP*V('Id')*SP*V('VarList')*SP ) * V('Content') * V('End') )/genDefine
      
      g.If = ( LEM( Cp() * K('if') * SP * V('Expr') ) * V('Content') * V('End') )/genIf
      
      g.Select = ( LEM( Cp() * K('select'))
		   * ( Ct( Ct( LEM(K('when')*SP*V('Expr')) * V('Content') )^1 )
	             + Cc({}) )
	           * ( ( LEM(K('otherwise'))/forget * V('Content')) + Cc(nil) )
                   * V('End')
                 )/genSelect

      g.For = ( LEM( Ct( Cp() * K('for') * SP * V('Id') * SP * K('in') * SP * V('Expr')))
                * V('Content')
                * V('End')
              )/genFor

      g.Comment = LEM( Cp() * P('¶') * (1 - V('EDel'))^0 )/genComment

      g.Inline = V('Verbatim') + V('InlineDirective')

      g.Operation = V('Include') + 
                    V('If') +
		    V('Select') +
		    V('For') +
		    V('Define') +
		    V('Comment') +
		    ((Cp() * Ct(V('Inline')^0))/genMulti * Cp() * V('NL'))/genNL +
   	            V('Inline')^1 * ( #P(-1) + #V('ClosingBrace'))        -- EOF or }

      g.Content = ( Cp() * Ct( V('Operation') ^0 ))/genMulti

      g.InlineContent = ( Cp() * Ct( V('Inline')^0 * ( V('NL') * V('Content') * V('Inline')^0 )^-1 ))/genMulti

      g.Everything = V('Content') * #P(-1) 
                   + (V('Content')/forget * Cp() * Cc('Parse error'))/genError 

      return g
   end

   local parser = DefineGrammar(delimiters)
   local evaluator = lpeg.match( parser, text)
   if not evaluator then
      return false,{'Mysterious parse error'}
   end
   if type(evaluator)=='table' and evaluator.error then
      return false,{evaluator:format()}
   end
   return genNewModelOp(evaluator),nil
end

--
-- Take a string in, return a callable which takes an environment and returns the string expansion of it
--
-- Input:  A string which is the template
-- Output: 1) A callable which takes an environment and returns a string expansion of it, or nil if error
--         2) An array of diagnostic strings, or nil if nothing to say
--
-- Does not mutate environment, but you might if things called in the environment mutate it.
--
function view0.expander( src, delims)
   if not delims then delims = { ['«'] = {'»'} } end

   local prog,parseDefects = parse( src, delims)

   if parseDefects then return nil,parseDefects end
   
   if type(prog)=='table' and prog.evaluator then
      return function(env)
		return prog.evaluator(env)
	     end
   end

   print(dump(prog),'prog:')
   return nil,{ 'Parse did not yield a valid program' }
end


--
-- And finally return our exported self
--
package.loaded['view0'] = view0
return view0
