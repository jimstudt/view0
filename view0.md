# View0 – A Simple Template System for Lua

---
**Note:** Consider this to be alpha software. I am still making incompatible changes with no consideration of anyone but myself.

---

View0 is a simple template system which acts as the **view** in an MVC system. You provide View0 with a model and template and it returns to you formatted text.

By "simple" I mean:

* Ships as a single source file with about 12 pages of code.
* Minimal dependencies. (Needs LPEG, but that saves a lot of parsing code. Sorry.)
* API has a single call with one string argument.
* Only six directives to memorize.
* MIT licensed to match Lua itself.
* One of these statements is not quite true.

## An HTML Example
Let's say you have a system with users and each user has some details. Maybe you want to have a user detail page. You will need a model. Models are nothing more than Lua tables mapping names to values.

Pretend we pulled this from our database.

```
model = { username='Jim',
          lastseen='Dec 6, 2010',
          privileges={superuser=true,upload=true}
        }
```

Now we are going to need a template. Pretend we keep this in a file named `userdetail.html`

```
<html>
  <header>
    <title>User Detail – «username»</title>
  <header>  
  <body>
    <h3>User «username»</h3>
    <div>Last seen on «last seen».</div>
	«if privileges.superuser»
    <div>Has superuser privileges.</div>
    «end»
  </body>
</html>
```
If we read our template into `userDetailTemplate` then we can…

```
require(View0)

local myViewer = View0.expander(userDetailTemplate)
print( myViewer(model))
``` 
…to get this…

```
<html>
  <header>
    <title>User Detail – Jim</title>
  <header>  
  <body>
    <h3>User Jim</h3>
    <div>Last seen on Dec 6, 2010.</div>
    <div>Has superuser privileges.</div>
  </body>
</html>
```

## The Template Language
The template language is inspired by Terrence Parr's [Enforcing Strict Model-View Separation in Template Engines](http://www.cs.usfca.edu/~parrt/papers/mvc.templates.pdf) in which he argues that Turing complete template languages are a mistake. Logic is best left to the *model* and *controller*, with the template engine acting as a *view* and simply converting from the model to the desired representation.

View0 syntax leans toward meaningful words rather than cryptic symbols for the sake of non-programmers who might need to edit templates.

### The Delimiters
The default delimiters, `«these»`, used in the template language are the Unicode "LEFT-POINTING DOUBLE ANGLE QUOTATION MARK" (U+00AB) and "RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK" (U+00BB). You type these in various strange and marvelous ways depending on your OS and language.

| Environment    | Language         | Sym | Sequence                   |
| :--------------| :----------------| --- | :--------------------------|
| Mac OS X       | English          |  «  | ctrl-\\                    |
|                |                  |  »  | ctrl-shift-\\              |
| Windows        | US Int. English  |  «  | Alt-[                      |
|                |                  |  »  | Atl-]                      |
| X11            |                  |  «  | ctrl-shift-U A B Enter     |
|                |                  |  »  | ctrl-shift-U B B Enter     |
| X11 w/Compose  |                  |  «  | Compose < <                |
|                |                  |  »  | Compose > >                |
| Linux Console  |                  |     | I don't know.              |

You can use whatever delimiter strings you like, and as many as you like. These can be passed in to the `view0.expander` function as the 2nd parameter. You pass a table where the keys are the start delimiters and the values are arrays of acceptable ending delimiter values.

Maybe you like ASP style variable delimiters.

```
myexpander = view0.expander(mytemplate, { ['<%='] = {'%>'}, 
                                          ['<%'] = {'%>'} )
```

### The Directives

| Goal        | Syntax Example               |
| :-----------| :----------------------------|
| Insert      | `«name.subfield.sub2»`       |
| Iterate     | `«foreach u in users»`       |
| Conditional | `«if name.something»`        |
| Define      | `«define header(title)»`     |
| Call        | `«header(page.title)»`       |

Some of the directives are multiple syntaxes to make them more convenient, and there is a `«end»` for marking the end of blocks that you haven't seen, but that is it. Anything more complicated, you should probably be doing in your model or controller code.

#### The Insert Directive
The insert directive is simply a list of names separated by periods. It will follow down your model, much like Lua itself would, and replace itself with the value it finds. A `nil` or `false` at any point means no replacement content.

If the value you are inserting is an array, you may wish to specify the optional separator, like this:

```
array foo = [ «myfoos;', '» ]   -- a comma separate list of all my foos
``` 

#### The Conditional Directive
The conditional is of the form:

```
«if VALUE»
Text and directives that are only included if VALUE is not nil or false
«end»`
```

**Important!** `if` and `end` are a special form known as a "*line eating monster*". It removes the entire line containing it from the template. This allows you to wrap them in your language's comments and not offend your fancy language sensitive editor or clutter your output with mysterious empty comments.

There are two alternate syntaxes for the conditional that are not a *line eating monsters*.

```
  Foo «?isPointer:'*'»bar;
  Baz «?isAtomic:'atomic':'not atomic'» bar2;
```

The first alternate is useful for short strings or single values and looks like `«?x:expr»` and evaluates and inserts `expr` if the condition is true. `expr` without quotes is looked up in your model, with quotes is a literal. (More on this in the Values section.) You can also have an *else* section like so: `«?x:iftrue:iffalse»`.

The second alternate is useful for longer strings and things which contain directives. It looks like 

```
«?x{» Some stuff, maybe a «call(foo)» or something«}»
```

This is evaluates `x`, and then expands the body if needed. The `«}»` is equivalent to an `«end»`, but does not eat lines.

#### The Select Directive
The select directive is of the form:

```
«select»
«when expr1»
  Use this when expr1 is true.
  It can have «other.things» in it if you want.
«when expr.two and expr.three»
  If the previous "when" didn't match, then
  this gets used if both expr.two and expr.three are true.
«otherwise»
  Use this if nothing else matched.
«end»
```

You can have as many `when` clauses as you wish, the first one to succeed is used. The `otherwise` is optional and will match always. It should be last.

#### The Iterate Directive
The iteration is of the form:

```
«for VARIABLENAME in VALUE»
text maybe containing
an insert using «VARIABLENAME.optionalsubfields» in it, 
or perhaps some «other.insert»
«end»`
```

**Important!** `for` is also "*line eating monsters*". See description in the Conditional Directive section.

If `VALUE` evaluates to `nil` or `false` then the iteration expands to nothing.

There is an alternate syntax for iteration which does not include *line eating monsters*.

```
array my dogs = [ «*dog:dogs{» DOGNAMED(«dog») «,»«}»];
```
You can see that `«*v:mmm{»` is the same as `«for v in mmm»`, and the `«}»` is equivalent to `«end»` except that they do not eat lines.

You'll also have noticed `«,»`. This is shorthand for `«?DELIMIT:','»`. `DELIMIT` is a special symbol which is true in your environment for each expansion of an iteration except the last one where it is false. You can use it to include separators. Notice that the separator doesn't have to be at the end of the expansion…

```
    «for v in main.arguments»
    Foo «v.name»«,»    -- «v.comment»
    «end»
```
The comma delimiter gets inserted as needed, but you still get to have a comment after it.

#### The Define and Call Directives
The `define` directive is used to declare a modular, reusable element, which can be inserted using the `include` directive.

    «define guard(name)»
    #ifndef «name»_IS_INCLUDED
	#define «name»_IS_INCLUDED

    «end»

	«include guard(headerName)»

A `include` temporarily binds the passed values to the parameters named in the `define` and expands the body of the definition.

Both `define` and `include` are *line eating monsters*. And note that all white space, include the trailing newline is preserved in the definition.

If you want to expand a call inline, you will want to use the alternative form which does not preserve white space.

```
«=title(text){»<title>«text»</title>«}»

<html>
  <header>
    «title(page.title)»
    …
```

There is one more way of using a `call` without a `define`.

```
/*  «include license.mit("Jim","2013")» */
```
The expansion can be supplied in the model.  Instead of a bare name which might have been created with a `define` directive, it has a name which is going to be looked up in the environment. This is expected to be a function which returns a string when called with the given parameters.

This is useful for quoting values and lazy evaluation. For instance, if you were making HTML you would probably want a function to escape strings…

```
    -- This is a Lua file with the template embedded.
    
    local template = '<title>«escape.html(page.title)»</title>'
    
    function model.escape.html( text)
        return MyFavoriteEscaperOfHTML(text)
    end
    
    myViewer(model,template)
```

You may find that you want to pass the entire model to a function you supplied. You can do this with the special name `MODEL` which evaluates to the entire model and local environment.

You will have noticed the quotation marks in some of the example call statements. Since we are done with directives we can talk briefly about values.

### Values
When you type a value in a directive, there is a decision to be made. Should it be looked up as a key in your model, or did you mean it to be a literal string? In most contexts, the "right thing" is obvious, but the `call` puts us straight into ambiguity and decisions must be made. Here they are:

| Context                   | Handling                                     |
|:--------------------------|:---------------------------------------------|
| «name»                    | Look up in model.                            |
| «if name»                 | Look up in model.                            |
| «foreach a in name»       | Lookup *name* in model. *a* is a new name.     |
| «define aaa(bbb,ccc)»     | All new names. No lookups.                   |
| «include name(aaa,bbb)»   | *name* is special, lookup others unless quoted |

Quoting may be done with either single or double quotation marks.

The name of a call directive is handled specially. It generally does the right thing, but it bears understanding to avoid confusion and understand its full capability.

* If you have previously (in scope) done a `define` with the name, then it will use that definition.
* Otherwise, it will evaluate name in your model…
* If it returns a string, it will look for an active `define` of that name. (Did I really implement this?)
* If it returns a table containing a `template` entry, it will expand that template. If there is a `parameters` entry it will be an array naming the parameters. This allows you to provide template language fragments from your model.
* If it returns a function, the function will be called with supplied arguments.

### Expressions
There are a limited number of expression forms you can use in your values.

* `=` equality test. This is types are first converted to strings.
* `and` logical and.
* Function invocation.

I should note that I'm not committed to these. That combined with having done a cheap job of parsing, it is just a regex matcher and wil fall apart if you put an '=' or 'and' in a literal.

If I keep it, it will need to get a real parser and a more complete set of operations.

## The Model
The model is expressed as a table of named values. Generally the values will be strings, arrays, or tables, but numbers, functions, and various esoteric Lua constructs are also possible.

When a value from the model is used in an insert directive these are the results:

| Value                     | Result                                           |
|:--------------------------|:-------------------------------------------------|
| string                    | itself                                           |
| number                    | converted to string                              |
| boolean                   | nothing                                          |
| function                  | call, no args, and consider the result           |
| table with a __tostring   | convert to string                                |
| table with a __call       | evaluate as function, consider result            |
| array (i.e. x[1] exists)  | iterate, concatenate results, optional separator |
| table (no x[1])           | undefined order iteration, otherwise as above    |
| anything else             | nothing                                          |

If you nest groups or functions more than 100 levels deep, the engine will give up and make an error.

## Formal Grammar

There are directives which consume their lines. To support this, there are SOL and EOL lexems at the start and end of each line.

SDEL and EDEL are the start and end delimiters for the directives.

```
all : directives

directives : directive*

directive : echo
          | if directives ( else directives)? end
          | SDEL expr EDEL
          | for directives end
          | define directives end
          | include
          | select (when directives)* (otherwise directives)? end
		  | SOL
		  | EOL
		  
if : SOL echo? SDEL IF expr EDEL echo? EOL

else : SOL echo? SDEL ELSE EDEL echo? EOL

end : SOL echo? SDEL END EDEL echo? EOL

for : SOL echo? SDEL FOR name IN expr EDEL echo? EOL

define : SOL echo? SDEL DEFINE name '(' names? ')' EDEL echo? EOL

include : SOL echo? SDEL INCLUDE name '(' exprs? ')' EDEL echo? EOL

select : SOL echo? SDEL SELECT EDEL echo? EOL

when : SOL echo? SDEL WHEN expr EDEL echo? EOL

otherwise : SOL echo? SDEL OTHERWISE EDEL echo? EOL

binop : ';'
      | AND
      | OR
      | '='

expr : name
     | expr binop expr
	 | '(' expr ')'
	 | expr '?' expr : expr
     | name '(' exprs ')'

exprs : ( expr ( ',' expr)* )?
      
names : ( name ( ',' name)* )?

echo : lexically special, text which is not in delimiters and not consumed by a line eating monster.



```
 
## Things That Still Need Doing

* Conditionals could use an *else* of some sort.
* There is a problem around the inline version of `define`. It passes whitespace, but if I make it a LEM it limits to a one line definition with my current parsing strategy. For now I'm leaking write space in.
* Maybe values should allow unquoted integers?
* Ponder the wisdom of having added equality and conjunction operators.
* Ponder if maybe `select` should just be `elseif`.


